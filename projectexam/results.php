<?php session_start() ?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title></title>

  <!-- Custom fonts for this template -->
  <link href="template/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="template/css/sb-admin-2.min.css" rel="stylesheet">

  <!-- Custom styles for this page -->
  <link href="template/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.html">
       
        <div class="sidebar-brand-text mx-3">Online Exam </div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
     

      <!-- Divider -->
  
     


     
     
      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <!-- Sidebar Toggle (Topbar) -->
          <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
            <i class="fa fa-bars"></i>
          </button>

         

          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">

            <!-- Nav Item - Search Dropdown (Visible Only XS) -->
            <li class="nav-item dropdown no-arrow d-sm-none">
              <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-search fa-fw"></i>
              </a>
              <!-- Dropdown - Messages -->
              <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
                <form class="form-inline mr-auto w-100 navbar-search">
                  <div class="input-group">
                    <input type="text" class="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2">
                    <div class="input-group-append">
                      <button class="btn btn-primary" type="button">
                        <i class="fas fa-search fa-sm"></i>
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </li>

            

            <div class="topbar-divider d-none d-sm-block"></div>

            <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small">User</span>
                
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="logout.php" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Logout
                </a>
              </div>
            </li>

          </ul>

        </nav>
        <!-- End of Topbar -->



     <div class="col-md-8" style="margin: auto;">
      <div style="margin-bottom: 80px;"></div>

      <?php 
      require 'perfect_function.php';
      if (isset($_POST['submit'])) {
        $answer1 = $_POST['1'];
        $answer2 = $_POST['2'];
        $answer3 = $_POST['3'];
        $answer4 = $_POST['4'];
        $answer5 = $_POST['5'];
        $question_group_id = $_POST['question_group_id'];
        $answers = array();
        $correct_answers = array();

        array_push($answers, $answer1,$answer2,$answer3,$answer4,$answer5);

        
        $correct = 0;

        $question_table = get_where_custom('question', 'question_group_id ', $question_group_id);
        foreach ($question_table as $row) {
          $answer = $row['answer'];

          array_push($correct_answers, $answer);

        }

         for ($i=1; $i <= 5; $i++) { 
            if ($answers[$i-1] == $correct_answers[$i -1]) {
              $correct = $correct + 1;
            }

         }

         $data['user_id'] = $_SESSION['account_id'];
         $data['question_group_id'] = $question_group_id;
         $data['score'] = $correct;
         $data['date'] = date('Y-m-d');

         if ($correct >=2) {
          $data['status'] = 1;  
         }else{
          $data['status'] = 0;
         }
         

        insert($data, 'results');

        $quiz_monitor_table = get_where_custom('quiz_monitor', 'group_quiz_id', $question_group_id);
        $quiz_monitor_count = mysqli_num_rows($quiz_monitor_table);
        
        if ($quiz_monitor_count <= 0) {
          $monitor['user_id'] = $_SESSION['account_id'];
          $monitor['date'] = date('Y-m-d');
          $monitor['group_quiz_id'] = $question_group_id;
          $monitor['tries'] = 1;
          $monitor['score'] = $correct;
          if ($correct >=3) {
          $monitor['status'] = 1;  
         }else{
          $monitor['status'] = 0;
         }
          insert($monitor, 'quiz_monitor');
          
        }else{
          $user_id = $_SESSION['account_id'];
          $sql =  "SELECT * FROM quiz_monitor WHERE user_id = '$user_id' AND group_quiz_id = '$question_group_id'";
          $quiz_get_id_table = custom_query($sql);

           foreach ($quiz_get_id_table as $key => $row) {
             $monitor_id = $row['id'];
             $tries = $row['tries'] + 1;
             $data = array(
             "tries" => $tries

           );
  
             update($data, $monitor_id, 'quiz_monitor');
           }
          
          
         
        }
        
        echo "<h1>Your results are: " . $correct . "/5</h1>";
    
      }

       ?>
        <br><br><a href="user_page.php" class="btn btn-primary">Back</a> 
     </div>
                  

       

    

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="index.php">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="template/vendor/jquery/jquery.min.js"></script>
  <script src="template/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="template/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="template/js/sb-admin-2.min.js"></script>

  <!-- Page level plugins -->
  <script src="template/vendor/datatables/jquery.dataTables.min.js"></script>
  <script src="template/vendor/datatables/dataTables.bootstrap4.min.js"></script>

  <!-- Page level custom scripts -->
  <script src="template/js/demo/datatables-demo.js"></script>

</body>

</html>
