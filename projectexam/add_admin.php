<?php include "perfect_function.php";

 ?>
<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title></title>
  

  <!-- Custom fonts for this template-->
  <link href="template/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="template/css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body class="bg-gradient-warning">
   
  <div class="container">

    <div class="card o-hidden border-0 shadow-lg my-3">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
          <div class="col-lg-3 d-none d-lg-block "></div>
          <div class="col-lg-6">
            <div class="p-2">
              <div class="text-center">
                <h1 class="h4 text-gray-800 mb-4" style="font-weight:bold; font-family:verdana; font-size:25px;">Sign Up Form</h1>
              </div>
              <form class="user" method="post"action="add_admin_proc.php">
                <div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
                    <input type="text" class="form-control form-control-user" name="username" placeholder="Username" style=" font-family:verdana; font-size:15px;">
                  </div>
               
                <div class="form-group">
                  <input type="password" class="form-control form-control-user" name="password" placeholder="Password" style=" font-family:verdana; font-size:15px;">
                </div>
                
                  <div class="col-sm-6 mb-3 mb-sm-0">
                    <input type="text" class="form-control form-control-user" name="firstname" placeholder="First Name" style=" font-family:verdana; font-size:15px;"><br>
                  </div>
                 
                  <div class="col-sm-6 mb-3 mb-sm-0">
                    <input type="text" class="form-control form-control-user" name="lastname" placeholder="Last Name" style=" font-family:verdana; font-size:15px;"><br>
                  </div>

                  
                </div>
                
                <button type="submit" class="btn btn-success  btn-user btn-block"style=" font-family:verdana; font-size:15px;"> Register Account </button>
                </form>
                <hr>
                <form class="user" method="post" action="index.php">
                    <button type="submit" class="btn btn-warning  btn-user btn-block" style=" font-family:verdana; font-size:15px;"> Return to Login </button>
                  </form>
                
              <hr>
              
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="template/vendor/jquery/jquery.min.js"></script>
  <script src="templatevendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="template/vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="template/js/sb-admin-2.min.js"></script>

</body>

</html>


